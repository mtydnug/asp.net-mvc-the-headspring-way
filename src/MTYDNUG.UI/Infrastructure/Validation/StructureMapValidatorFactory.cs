﻿namespace MTYDNUG.UI.Infrastructure.Validation
{
    using System;
    using FluentValidation;

    public class StructureMapValidatorFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type validatorType)
        {
            return StructuremapMvc.ParentScope.CurrentNestedContainer.TryGetInstance(validatorType) as IValidator;
        }
    }
}