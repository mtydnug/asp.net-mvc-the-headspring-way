namespace MTYDNUG.UI.ModelBinding
{
    using System.ComponentModel;
    using System.Web.Mvc;

    public interface IFilteredPropertyBinder
    {
        bool ShouldBind(ControllerContext controllerContext, ModelBindingContext bindingContext,
            PropertyDescriptor propertyDescriptor);

        object GetPropertyValue(ControllerContext controllerContext, ModelBindingContext bindingContext,
            PropertyDescriptor propertyDescriptor);
    }
}