﻿namespace MTYDNUG.Core.Features.Employee.Add
{
    using System;
    using Domain;
    using Enumerations;
    using MediatR;
    using Services;

    public class EmployeeAddHandler : RequestHandler<EmployeeAddModel>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeAddHandler(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        protected override void HandleCore(EmployeeAddModel message)
        {
            var employee = new Employee
            {
                FirstName = message.FirstName,
                MiddleName = message.MiddleName,
                LastName = message.LastName,
                DateOfBirth = DateTime.Parse(message.DateOfBirth),
                Address = new Address
                {
                    City = message.AddressCity,
                    State = State.FromValue(message.AddressState),
                    Address1 = message.AddressAddress1,
                    Address2 = message.AddressAddress2,
                    ZipCode = message.AddressZipCode
                }
            };

            _employeeService.SaveOrUpdate(employee);
        }
    }
}
