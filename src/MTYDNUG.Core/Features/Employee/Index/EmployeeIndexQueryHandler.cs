﻿namespace MTYDNUG.Core.Features.Employee.Index
{
    using System.Linq;
    using Infrastructure.Mapping;
    using MediatR;
    using Services;

    public class EmployeeIndexQueryHandler : IRequestHandler<EmployeeIndexQuery, EmployeeIndexModel>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeIndexQueryHandler(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public EmployeeIndexModel Handle(EmployeeIndexQuery message)
        {
            var model = new EmployeeIndexModel
            {
                Employees = _employeeService.GetList().MapAll().To<EmployeeIndexModel.EmployeeModel>().ToList()
            };

            return model;
        }
    }
}
