﻿namespace MTYDNUG.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain;
    using Enumerations;

    public class EmployeeService : IEmployeeService
    {
        public IEnumerable<Employee> GetList()
        {
            for (var i = 0; i < 20; i++)
            {
                yield return new Employee
                {
                    Id = i,
                    Username = "username" + i + "@mtydnug.org",
                    FirstName = "First Name " + i,
                    MiddleName = "Middle Name " + i,
                    LastName = "Last Name " + i,
                    DateOfBirth = DateTime.Today.AddYears(-25).AddDays(i),
                    Address = new Address
                    {
                        Id = i,
                        City = "City " + i,
                        State = State.Texas,
                        Address1 = "Street Name " + i,
                        Address2 = "Street Number " + i,
                        ZipCode = "Zip Code " + i
                    }
                };
            }
        }

        public Employee GetById(int id)
        {
            return GetList().SingleOrDefault(x => x.Id == id);
        }

        public void SaveOrUpdate(Employee employee)
        {
            //Save Employee
        }

        public void Delete(Employee employee)
        {
            //Delete Employee
        }
    }
}